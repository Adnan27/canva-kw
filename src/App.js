import React from 'react';
import './App.css';
import logo from './canva.svg';
import {
  Navbar,
  Nav,
  Button,
  NavDropdown,
  Form,
  Jumbotron,
  Container,
  Row,
  Col,
  Image,
  CardDeck,
  Card


} from 'react-bootstrap';


function Header() {
  return(

          <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">
          <img
        alt=""
        src={logo}
        width="60"
        height="60"
        className="d-inline-block align-top"
      />{' '}
      

          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              
              <NavDropdown title="Templates" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
              </NavDropdown>

              <NavDropdown title="Discover" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
              </NavDropdown>

              <NavDropdown title="Learn" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
              </NavDropdown>

              <Nav.Link href="#home">Pricing  </Nav.Link>
            </Nav>
            <Form inline>
             <div className ="button">
              <Button variant="outline-secondary">Log In</Button> 
              
              <Button className = "B">Sign Up</Button>
              </div>
            </Form>
          </Navbar.Collapse>
        </Navbar>
      
  );
}
function Welcome() {
  return(
    <div className="a">
    <Jumbotron className= "d">
    
      <Row>
    
        <Col>
        <p className= "p"><b>Design anything.</b></p>
        <p className= "P"><b>Publish anywhere.</b></p>
        <p className= "descripsi">Use Canva’s drag-and-drop feature and professional layouts to design consistently stunning graphics.</p>
        <Button className = "button1">Start designing for free</Button>
        </Col>
    
        <Col>
        <Image className ="img" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/about-banner.jpg" fluid />
        </Col>
    
        </Row>
    
  </Jumbotron>
  </div>
  );
}
function Judul() {
  return(
    <div>
      <p className="judul"><b>Design presentations, social media graphics, 
and more with thousands of beautiful layouts.</b></p>
    </div>
  );
}
function Feature() {
  return(
    <div>
      
        <Container>
        <CardDeck>
  <Card className ="Card">
    <Card.Img variant="top" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/facebookPost@2x-tb-238x0.jpg" />
    <Card.Body>
      <Card.Title>Facebook post</Card.Title>
      
    </Card.Body>
    
  </Card>
  <Card>
    <Card.Img variant="top" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/facebookAd3@2x-tb-478x0.jpg" />
    <Card.Body>
      <Card.Title>Facebook Ad3</Card.Title>
      
    </Card.Body>
    
  </Card>
  <Card>
    <Card.Img variant="top" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/businessCard@2x-tb-478x0.jpg" />
    <Card.Body>
      <Card.Title>Businnes Card</Card.Title>
      
    </Card.Body>
    
  </Card>
  <Card>
    <Card.Img variant="top" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/gPlusHeader@2x-tb-478x0.jpg" />
    <Card.Body>
      <Card.Title>Google + Header</Card.Title>
      
    </Card.Body>
   
  </Card>
  <Card>
    <Card.Img variant="top" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/facebookApp@2x-tb-478x0.jpg" />
    <Card.Body>
      <Card.Title>Facebook App</Card.Title>
      
    </Card.Body>
    
  </Card>
</CardDeck>
</Container>

    </div>
  );
}
function Title() {
  return(
    <div>
        <p className="judul"><b>It has everything you need for amazing design.</b></p>
    </div>
  );
}
function Program() {
  return(
    <div>
        <Row>
    
    <Col>
    <Image className ="program" src="https://about.canva.com/wp-content/uploads/sites/3/2015/06/about-banner.jpg" fluid  />
    
    </Col>

    <Col className= "col">
     <Navbar.Brand className= "logo">
          <p className= "desc">
          <img
        alt=""
        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX///8AAAAwMDDo6Oj09PTPz8/w8PDq6upRUVHj4+PU1NTZ2dnt7e3Dw8Px8fHd3d0NDQ0VFRW1tbWSkpJBQUFjY2OlpaVKSkolJSXIyMiCgoKenp4gICBycnJXV1eLi4s3Nzdzc3N7e3u6urpnZ2csLCxcXFywsLBDQ0Obm5uvhin8AAAFyUlEQVR4nO2daVdiORBAZQcBm0UUWbQVW7v//x+c4TAML1ul8pJUUn3qfjV55h4e2aoS7u4EQRAEQRAEQRAEQRAEQRAEQRCqZrAed3kxXg8C/FbLDkcWK6TfbFy6qa0ZzzCCvUXpdkaw6PkFhzzf0CvLodeQ7yt6YewTXJVuYTS+7mZbuoHRbGHBQen2JQAeF99KNy8Bb6DhY+nmJeARNOQ8Fl5ZgIalW5cEMVTZnCa9mpmsvuIMX8DSdTCPMeQgaCqChbVXlKiJsYxbG86JWhjLd2vDPlELY3n56w0HYthADOtEDJuIYZ2IYZO6DCcv6+Nms/ncfd+D5ZgaPuybWyqHI7DBxNJwdOzobJ3TZI6Ga8PvzPMPe2l+hqOfVsF/ebKWZ2cIbUt/ICqAD6/B0NiVUPi01GBmeA8KWj9FrQr4+PKGfY+g7bvIy/DVa9gxelRWhpj45bNeiZNhHxW/PGm1OBlqu2YO9CAoJ0NkCFqbvzEy9I0UVz7BauC/KGyIDkGr1RgZdrGG6lKKj6F/tL+ijvp8DLFfw07nHaoH/o+yhlr8AeBVqcfHEF5VNFGnNWLYhIuhmp83ZWOI/x5+KfX4GP5AG+6UenwM8ek8aoolI8NnrOFUqcbI8AkpuFSrMTIcIQ21BEtGhphdmjMTtRYnQ9x48aXV4mSI+xCnWiVWhlpjrRh7wqwMHVGnJgejWbwM/WOi/o4acyHw8ckM+/P1+/HtG3XSSqvpDK1d0DdL78oY7q+np94RR600ZuCBgW9LDXrDXrON4SmcfeDslfVp5IY99SmW18qHa1fxp/2NoDbs/9La1SKXemr7GBd7R2lqQzN5Hs6GsTPXHZfrB1dZYkPbGzbxVzOZ7Bu96gf0daY1tEePRi2edGY6X61Wp4Hz07tAaujIo+j6D69GQGk4ch0N+51zfkRp6J6PGKHphBAamt3oDX1RlxA6Qzj89+5/QEvIDH15FDv/I9pBZeiPja1jNAAmNIbObrSBPbUwGiJDz7LuQotZOAIaw0+MYKYTjSSG6CyKNrNwHxSGAddpODKZYyAwxOwBXjmE72v4yG+IzxI5s4VXCqeP8eYpbC2S3/B3kGHnF/Col8OlTNDsILuheTTCg3sW/oYoQ2/4J1TQPQtvztxfHWXoDU/hgvaM+7uRuoWFPyuf1xCfXaBg+Z4N9GnfsQrDh7bXLhmzcMuQCt+lQ2SIzi0w0Gbh1pjTn/KGwd1oAyUC4ZjW4hYjGQ33EYLN5g+dIyrqKsR8hvg8NDvX7ga6iRIT2NECJekMW3ajiuPpfvAE5z4jLu3MZdg/xBtiMGO+VIbtu9FAvIuRTIYfVIKdpSdskckwrhsNY+vpELIY4pNdU/AIK+Yw1J6ZHfhW0hyGRN3oDeti5IqW0pjCEJlCmBIo6pHecEcndgPY10huiDsGmRx31CO1YbF7ap0LjcSG2kSeElvGV3pDX2JdVhwLjbSGUCA7P/aoR1LDIt1oA+tCI6VhoW70xsKWXjVTy8QYVnDd98GSlJvOEBPIzo6Z5p3QEH3UOivmQiOZ4YbOAsSIaKQy9J8ToEKPaCQyLN6NNtDWUmkM8WflKVAXGkkMH+haj0JZaAzVv7UzrO5HE5oLjRSGyHwgShoRjQSG9XSjDW4LjXjDVoHs/Pwf0Yg2TBCBycM1vSrWcFbvb1z9t9DQOvpgQ7IITDjbUQrDmEB2drr9eMMW+UCUdKMNYwPZ2XmONKy2G72xMXIjQwypAtlRHGMMA9MqC7HTmh1gWOFs1IqWZB5gyBQx5I8Y8kcM+SOG/BFD/oghf8SQP2LIHzHkjxjyRwz5AxrWkLgWywI0rC7logWPoCH6ipKKgc9H0x5nyoPnSGbbw+f1oP/clU7ARTOV4j0bDdz/ygL4lNuZYb25JRiWiCsoe5zHxAXqAqMZ3xf1FXu79onnm7oMuQ1usB53eTFeI64mEARBEARBEARBEARBEARBEARBKMk/S2F5tfhDoAwAAAAASUVORK5CYII="
        width="40"
        height="40"
        float ="left"
        className="d-inline-block align-top"
      />{' '}  Lorem Ipsum is simply dummy text of the printing</p>
        

          </Navbar.Brand>
          <Navbar.Brand>
          <p>
          <img
        alt=""
        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEX///8AAABWVlaenp7V1dX4+PjMzMzJyclLS0u7u7va2tqRkZH8/Pzo6Ojl5eUdHR1oaGg7OzvCwsIuLi44ODjw8PCkpKR5eXlFRUXm5uaXl5dRUVGAgICKioqqqqpfX18kJCQcHBwODg5lZWUNDQ1zc3Ozs7MWFhY71oH5AAAFSElEQVR4nO3d61riMBAGYHqEQmUBOS0gigre/x2uVYS2aZsZ0zRN9nt/7gOY2UKOk2QwAAAAAAAAAAAAAAAAAAAAcF80DGaj7Xzrby5j02XRIfG9vKep6QK1Kz1+eGXPLj3IeCnElzlEpgvWlkllfJ9ObjzG9LkuwE+J6dK1IPUbAvS8i+nyqXtrDNDzYtMFVFX7G7xZmS6imlgaoDcyXUY1c3mE3tF0IVVcCAF6J5ubxVdKhF5gupi/tyAF6P0xXU6qeOOXPJZjGQXheHEQQiy/rwMbfiu1Gskf1rUHE81oz1avPbOZeiB85n3A9KS9/BQPrAgJzULCe7l+c06AhDplm389oSfQgQUjwqbxw9Uu//r0RX/55Z4ZERIKXKy8ZB3yTrwwIvwj/7iw8Iaz/vLLcRri6mmKguJgl9C26LdkRCgfJHnn/OtX+otPsKsLpwKlxPkWlvA/0gFWm7+Wf16u5prqLz3BmhPgYLCTf+LmFiBtzKEZ5zv6JZRXHstFNhRc9eIrOvrNBNE0LBqL3c8P/20r/rWwey0tKlC645/e2vlrRtCGSTbPfJPqzJnpUiqhDATtXmdLhYkMAbNV6h1pZWP3dzQzbA5wb7p8LWgc/u9tng2+CV3+in6L6pYQba9kci5Vsxyzv6aL1ab0WI5xZnNPptp4N3+/Rnd6uzhRw4iicTwcxiFvrhkAAAAAAKBr0d8kmEwmu2NoedZqtXRxzg2z/cCpKYRMIMyUuDWNkLyX48ucnZlKiGrTc4ami9aOacPqiBMzllNxU1XOxHTx1MkWf9gZCb0jXcDjpCD20UYWoO2bViSrd19sToUgZhvb3GYklACL+cmWqcg/qtJO/y1Kgs6J2XSPh2Mgrlc+r0tvTH7RnyOk7um3/P7FrcRdKwJuMynZKtqRffpTHsKeMk4e+6Anedteei8QIVfpXB+OqCEboUP5VPKUkFzPqX8IHYsOFGoPwkPc1IVTQZ7c1QG/UCTCNp5HRoQn3aWnKP6uxvI3cPZbuP8M3f8d2lmXhrXxVHC+PSTtz9PvnupI6dOkTfFUCPQHIKexXzroz9hivdsL/9rK2MKMTseHRtDG+FafLuL8PA1prs3yZGt5d+rd7vnS/2DOW7puYfHhMD+cX3v6D9YPm86Dsf6wtJvqdfyNNX0ziopcDLt3NopK+TRr5/JpMtGD0zlRAAAAAAAgl2b7vIexa0O6H9Ng+3PA2Yd/dG/kk5Rn5n13Zlcyi6pTJX2LF4tK0rrlcSemOT9N64923ToxT9aYwvHoQI0jSVtaWh/iSpbyYvXSbUZMKCg7mC6imqM0QGbWUt9EhAB5x6b3De1sfYuvSyI9wr5VNuUzaJuMK5LE/FlF3ZP05wzaWO0Q8tNXHtaKcKZ0O/jnCCsef3zL730gHO/eDmbqnmJmYu7ntmrMO2gTa4mfeCZyrXy7R7pCqRWcXqHid7R4imk7xSfgjNAUfzzFn0RnF+1w7kYgpI03KbbrnZ3ZztlvoXipSvEWREIKczteGBEq5rEXNz50tj2Fsz+PeP9YnZf8jIVqvUzHSlpUvMApnz/YWUXDG7ioXqty70N11m97Z56jupKP2Btd73lMO6tmuPeuDaruzmsifK2Xu3i8eBJuLxnpuTvvoH9anbA98IvFs6a0vSA2z0XRLpez+lRlyg5+u1cvCBeYfJguoyL5QND6NTbZhKID15I37611YCtI80DJ6tus72oXL16dWcufVk+znq1fO8xJxBHv3pkHeDUsDgkPVq+p1YnX5/12vt1vjk6GBwAAAAAAAAAAAAAAAAAAoMc/VR1RJ2PIk3oAAAAASUVORK5CYII="
        width="40"
        height="40"
        float ="left"
        className="d-inline-block align-top"
      />{' '} It is a long established fact that a reader will be distracted</p>
        

          </Navbar.Brand>
          <Navbar.Brand>
          <p>
          <img
        alt=""
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSl8if4spwc3xoAhvUfZhXOMAX8axU8CAk22g&usqp=CAU"
        width="40"
        height="40"
        float ="left"
        className="d-inline-block align-top"
      />{' '} Many desktop publishing packages and web page editors now</p>
        

          </Navbar.Brand>
          <Navbar.Brand>
          <p>
          <img
        alt=""
        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX///8AAAD7+/v39/fp6enR0dHz8/P29vbk5OTv7+/s7OzZ2dmPj4+vr69VVVXx8fGdnZ2+vr7X19cyMjLf39/Nzc15eXloaGjBwcHOzs64uLiWlpY7OzsZGRmJiYmgoKCrq6tJSUlaWlpDQ0MqKipzc3NAQEAbGxsMDAwiIiJ/f392dnZkZGQ1NTVtbW1YWFi9dnIfAAAOtElEQVR4nO1dCZOiPBN+ORRUUEFEBeVQVMBj/v+/+7rRmVHoYPDY/djKU1M1u+UQcnSePhP/+09AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQOD/EHIVf7tLb4SsaN3uaNTr9b/R6/VGo672r4xS7vZU3QEMAE7xM8D/6mpP+yeGKI/0wdSL/CDP3YN7gB/XzXNjbM4XA733DwxR7upTP05Oy2+cTpNJkkmStAusjj5q/RBlRe0E2dI1jAAWEeG67mE7i2GMe9ez1dYLqjxyzGy/NfLZehgDNgj4vUtgFTN/7vSVv93F1yBramcrTYabLJUIHM2O3vJFhCX0Mmm/p4YHSMO503KyUdRBwBhdgYPXUbW/3clXAEs4ndSNMImmTqvpFJZwTG7AH+SWrbaYa1AXHmsHKA3Nqd792/18HnLPMWuFFMTUnw9azDWgKvJ6IZXSc5u5BnjGGtYPUJI20by9XCP3B37yaIQZcE2/rSPU9MX20QAlad1ergEhjZaPRzgxWss16FU84BlEuvUW7VSJoAyt9eMBSlIMxmm3jYsIPDPOeEa4Dzy7lT6Upk85eAZxjIBr2reI6Dc9VBUXTPxW+lCKavt8A5SkLdg1rRNT4Jl5TAyGJNcd+FCtE1O5N4hIiSTJx7DaxzVgdJ+psQyphb3YNe1aRDS6qdVKz0dKTvfti9eAc29Qi7U8HEhDzm2bDwU8s6DEMT0GASm8u7bFa5BnKCFd5oYfUF7/KmiXDyWD30Qu1doIo5CM3Ky9VvlQGJ/ZEcNI3NCzvIBa3Um7uEZT7YAKc8eGOZ/OQyqysWqVXYP2DOU3pbPQ6ww63pZSGHEIXPO3e84L8JtCik6SPJo7+mBukB8G1uAPcs1rZQXAMzkxBin2zYXe0xcmGYCbwYd/SCXKstYtlxV0Ff5Bot8UU1ttFoJK0PqD+WFFfLz0/1RY8VJYMLA7V9i2PWhUVaCoHYPaaZMAXQil60zHJ+LjNP9DXCN3VdsKA/ewPV+wdQM/shaOymkas3hGGo5xCJj29sjPv8I/Yn7LwPT+JomLlHS82eyWkyyV0qE/HXCm3NGeoVTF6hAVKg8+nweUmGbINZ9fRKXnRFly8LE45FJUcD5uVqiRp5wpd0WleWZiXF2krj6NKDEFrul8nmtAxDpDKQ7cdbzbfdeHTHDGh96CS4bkrmOR/V+H3iVVqPRti7TcTuEf4BqwtzwpXS6rYpYZcy4ZUvqDMdV9KY+udhlw7ZwO4eSfd/XBZLbPUkoGU9APfyxDKAQx9Xji/+QnwLXyyLRi/HlXvy4EeOLKZaLRTU5QoQwvj8MyW6TrIY0/btfUlU4UtvHDRQQqPlBPp8GvjyujSqTYVJpxbvanga+OWSMs8guPiAD3GMkzG9/8nR9QiSb5no/7UGgys+PUCYcfjklRcnXO4Y0Agkq0SFkBu+azpRmaujjX5MMwv/BATEHXkfZKEtwGfbFGg57KD3MNy2T+xmPbGHiGVuZD/24To5jSMwF09kGuUcA1J0XsivRhzA94xqWEYIVCerPB5BFYbtQfpgXXfGqA8ohhMv8A/fA6GWIKwTKI7gNNGojphvrLzSe5Ru47NTxTzLBRzzWgbHwyL7EelxhEVm1vRv3l3rVs9VMjhHklTeYboB9eI0NAIKQmz7blohnUKgY5nUOkpM8MkBCxtFz3mozruAZ5hozZ74JKRQlMZ0QGMz5YmkG45tmwLHNunb4CZUPTBwppSbrRwSBjbhKWZnzEh6Jc8+W5TP27Gn2FfhO5LJlbrexC84kWUx7T6bkR9p2opCrSoTssT3ONvoJlCekUr0GE0RS9Q8fcVnllwd+DouT8HpNtnpen+egxFxF4hqTH9BgSFCyzYm7S+jOlGUhu5U03DAy/LLgJs5QQmYqsWM8CfKby52i5kV7i/jOlGWDPlF3zbOuHYZl8CruG5BoQAtrz2oxNijuYMTfpIyWnOKPlXbE0ItOLypZHzKibAOqY03V6d27FzQMYcyMfmEQfKANDVVYSMdg+3nzulY2AJKC5Bhz3kOzvPqDnBCfVpG2oT5RmgMlc5pkVbJ9OZ14dONjGROS0q3foovVLIJh4JzPm9omSUyydKKc0d745dZwqpe9Iu4Yd4XFDhpFSWG7kI9n47VwD02mUeRBDR6pa3SvFibPK64GpaKM2MZhGgqYvGKX8XCGhJpAJ1zw1MHQ0Akov2zWUvkKeicnOVtyKm4f6A4sMW0nLd5dmwKsqrnmM20fT+rZXdheoXCYWk5JCmuZsIwwtNzqqs393eTuIy6Fsb22L7YN7pZKSn5nTkgzJmkrbM6ByagLJGivm9u4yMMo1n1wdHnTGy59VuYZVfAFWnl9TAIwxN5eemPcepZH7tl9eJ+wZbh/4zCvrkbRStsyM8OzdOk+hEFPS0sPw8xvzUOA3lfda6l7jJUghFS/nq8Q1zKSotAnqxE1GPzgmH3wUEmoETGmWSfs3dKTpnYozvvfvuQZaoHlGWqNS7XdZ6DkLk85gZO8sA1PANS+/4Dj+FkSk9Iozfr6XIVgKmvX3Z9+c247OgmPPzYCemyMy1HuGCCJW4Zlbr3xEVBbcH/3E89o0z0zOgQ/mu8WCZ0bjoPzyC07vK80Ak7mSr4yB47/VNManK854bt3YmhjhIXsppYB9kkyY2B3d/EifymC6aY1BnPNMj2PL/iZr9HLyMlHe5heQZ1iH8NIkXh9nNLDOI/cj/4uOsw/fVZqBee3yKyb5jYeGJl2FifbjX30FO5VOin6P8MgaI46yqIOgsHpXGRhlMhflWbdCWD3AdP6N+YFlQvPMZYxpumKDTqhfcHxPGRgq3bJrvjreeeWwylVnfPmjyTGIyHk4phmy94QVKdc8ye/DCJQzvv+pm2hyOKYZ3nOSvasvKhJ4cStuZkG1rYpdPTQv+QXcpvFnRrh8h/mNJnN5edJzyStHB6NSI/OdX0DPqy7p+AqMN3CNBjxT3uxZUE7/aGrVGf8+Zt7FCpwPoQgJvTZAXJ2KKtv53v2dKjLGjCpiejn6iS1wHPZ9DpPX4zUK5ZrPYBvqI+0GWJBZsVoyzC8oIASVCM/bsDoA17y2iGRKMxh704Gu3kB3Fla16rU4+vkwM/4SXi7NwDhwxWTeBWPTml6Kgy/odBZzs5LBwKOfA1UdVCJVb0TyKtfgvRWVVtEIyZLJabncIS63c8WzfFsZCdjGjrOosWdeRvqqXTNyPMJkTpNlPPzF13p93BpRdKhs2GE0t1mhpDeBDj9zQ+7bY4Il0uXXbLs9HFxEfoW7rlpmy7E1nX6OZxD713wolmsOUpolBdCFK/6RUUr95Htz78ENO6/iC3Tz066+PBqYL6myk2F6Ph2WTza3gs6BeENb78g1vWcXUeszUpq82MHupC8V2K/zPDCaIMjXtCN1INOrfEvIOE/Oj6ER+rQQLHM/jMwmiMYB3dTueR+KmdLkxtkY0/mm1dGPrOmiw4/p1IvIM9CvlJxquv0aS0xc34/pT4LQWjh3ZtEDoNVEHmkrysCes2vQZOa4PaYGw8Bn3D8zHINTMNIUfmg9Z0qX10jpsykMjXGenBv7meHT+SbMqDU1tmDP0BWnlfAzL17nmV1uMORq6Tc3mGXmWaGaEp76FoFnXrJG0nVgMEzSogaqKTmw6xawNOMJrgGeoUWMF8nWYPDMKnhm0pEXGNw+fMaHwqRoRf9MjrPz9mKOUjjgObYfQYoDgzx2Xg1kcUJTF9UuFcieKW8nU5qzwPDHtfCNbxsGeMZgWCE5XQP1CFjKRyeE01nzKSNTmplRmye6pIr86yKecoNhhJzY5SWP+sQ4hCEtm4s9eZ58jYaIPaiBPZ1Hl6gH8syWFtLf1GNDgFyZNNdkzY/tdQm/aQXS1XFUPLrNQL+v29bFmdiDqqAVdJY/e9VjUSNFC37z0oyeU3XNY9RiI4W44v8HSldfXKYZeCanlWFsPOsMYFY/pDOtJ6Mh18iq7Vcma8tBEMWRrAxvAgz8GW0oz54/Q6gQdR/XVrEMrEGjyDMVgT+Vj7WQT8I0m7M0PYJJWhEC/maY/XLmjOtA43GjklN5ZIcVljiy689uHwVKj4Lc8H2XtojWrFpLHihqJ6Kjr/vAsvldfSzRqigeToKA5bfnZhSGY7onGcr608ExrPvI6Zk7NuEa0p4pahM4CELu6aAzPJMhTTvjpeLXrsM48CVNfIs/hUHdw4UEYfO0ICsj1bGnleLoazPH8KVDoFjdzjCXDyZ3w+Q9XKec92I/Wdb6esejZzp5WhleG8dSSFo6NvxlYMj4leeLM57cU6Ta5I2B1xKHF6LwOPuV0wFX8eC88eC/yznP8uP7ctq3thtMzytlHDxoMETwEhlHkfE2MK7Jw6L1ijXSiCCwBTqvnfjPK8OftqeMIsA04kyXYpFXeZLSdROCgBYYER7mwQN+aMA1jAKrgLNxkPSKPdOIIKjjNdeJepFnitZ7rCMKUswXVkRlWLFuGxEEtlAXgXppgMX8MZrn/JIB6tqE1G1CENgC7Rm+zDMILN5hmN8B18FEPKNWnqJho+gftEC74hvjHZflIteEZPRgxXdVloyO9B3TZGujuGWswQgpuyON8/DlygkEaDPPoIYY+1zhGqVwDmZfxUVX8fC4zfEmr04DD/rSwhm/vmmzuX5909dsmxsh+3hpE8gj2Inj4gXY+O7yhuF660dcbjA+b0Uhxs0A8CsMTavj9BTurkELHcv8buECbCfyps7TucxbwBROPeyi79+9AV7A8yVmYFU6nbnl/cCaT229Uc8UdC+KFq65v2szHafPP098XfxJLxZvgBfw7CVZ6/3eqHe9T6/baOoL92Jg2zfpv0s77/qyv2sX7bsMY4MXyEp31O9f83b9frM7ES8tyNqo17/L/0E73e7bvrARu1h6AbwBO8rdwyJwpihPf1vmTwtF7u/5dh68QdF+36D8W9/rKSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDQUvwPjqyAINuKl20AAAAASUVORK5CYII="
        width="40"
        height="40"
        float ="left"
        className="d-inline-block align-top"
      />{' '} Various versions have evolved over the years, sometimes by accident,</p>
        

          </Navbar.Brand>
    </Col>

    </Row>
    </div>
  );
}
function Review() {
  return(
    <div>
      <Card className="text-center">
  
  <Card.Footer className="text-muted">© 2021 Copyright Canva</Card.Footer>
</Card>

    </div>
  );
}
function Belajar() {
  return(
    <div>

    </div>
  );
}
function footer() {
  return(
    <div>
        
    </div>
  );
}

function App() {
  return (
    <div> 
      <Header/>
      <Welcome/>
      <Judul/>
      <Feature/>
      <Title/>
      <Program/>
      <Review/>
      <Belajar/>
      <footer/>
    </div>
  );
}

export default App;
